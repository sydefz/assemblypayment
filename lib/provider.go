package lib

import (
	"log"

	"github.com/tidwall/gjson"
	"github.com/cornelk/hashmap"
)

// RequestTimeout TBD
const RequestTimeout = 3000

// Provider TBD
type Provider interface {
	GetResChannel(string, PubSuber) chan interface{}
}

// WeatherProvider TBD
type WeatherProvider struct {
	url             string
	process         func([]byte) WeatherData
	pendingRequests *hashmap.HashMap
	dialer          Dailer
}

// GetResChannel return a res channel and start calling weather provider if necessary
func (w *WeatherProvider) GetResChannel(city string, pubsub PubSuber) chan interface{} {
	if _, ok := w.pendingRequests.Get(city); !ok {
		w.pendingRequests.Set(city, struct{}{})
		go w.callAPI(city, pubsub)
	}
	return pubsub.SubOnce(city)
}

// callAPI making api request to weather provider and Publish the result to the "city" topic
func (w *WeatherProvider) callAPI(city string, pubsub PubSuber) {
	var data interface{}
	if bytes, err := w.dialer.Get(w.url); err == nil {
		data = w.process(bytes)
	} else {
		data = err
	}
	log.Println("try pub on", city, data)
	pubsub.Pub(data, city)
	w.pendingRequests.Del(city)
}

// NewProviders return primary and failover weather providers
func NewProviders(dialer Dailer, dialer2 Dailer) (*WeatherProvider, *WeatherProvider) {
	apixuProcess := func(d []byte) WeatherData {
		return WeatherData{
			WindSpeed: gjson.GetBytes(d, "current.wind_kph").Float(),
			Temp:      gjson.GetBytes(d, "current.temp_c").Float(),
			Primary:   true,
		}
	}
	openweathermapProcess := func(d []byte) WeatherData {
		return WeatherData{
			WindSpeed: gjson.GetBytes(d, "wind.speed").Float(),
			Temp:      gjson.GetBytes(d, "main.temp").Float(),
			Primary:   false,
		}
	}

	return &WeatherProvider{
			url:             "http://api.apixu.com/v1/current.json?q=Sydney&key=937ca2cedda34e85ad3113907191907",
			process:         apixuProcess,
			pendingRequests: hashmap.New(1),
			dialer:          dialer,
		},
		&WeatherProvider{
			url:             "http://api.openweathermap.org/data/2.5/weather?q=sydney,AU&appid=2326504fb9b100bee21400190e4dbe6d",
			process:         openweathermapProcess,
			pendingRequests: hashmap.New(1),
			dialer:          dialer2,
		}
}
