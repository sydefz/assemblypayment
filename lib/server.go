package lib

import (
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/cskr/pubsub"
	cache "github.com/patrickmn/go-cache"
)

// WeatherServer TBD
type WeatherServer struct {
	PrimaryProvider  Provider
	FailoverProvider Provider
	Cache            Cacher
	Pubsub           PubSuber
}

// NewWeatherServer TBD
func NewWeatherServer() *WeatherServer {
	dialer := &HTTPDailer{
		timeout: RequestTimeout,
	}
	primaryProvider, failoverProvider := NewProviders(dialer, dialer)
	return &WeatherServer{
		PrimaryProvider:  primaryProvider,
		FailoverProvider: failoverProvider,
		Cache:            cache.New(3*time.Second, time.Minute),
		Pubsub:           pubsub.New(1),
	}
}

func (ws *WeatherServer) runAPIServer() {
	http.HandleFunc("/v1/weather", ws.GetWeather)
	logo := `
 __      __               __  .__                  
/  \    /  \ ____ _____ _/  |_|  |__   ___________ 
\   \/\/   // __ \\__  \\   __\  |  \_/ __ \_  __ \
 \        /\  ___/ / __ \|  | |   Y  \  ___/|  | \/
  \__/\  /  \___  >____  /__| |___|  /\___  >__|
       \/       \/     \/          \/     \/       
               _____                               
              /  _  \ ______ ______                
             /  /_\  \\____ \\____ \               
            /    |    \  |_> >  |_> >              
            \____|__  /   __/|   __/               
                    \/|__|   |__|                  
	`
	log.Println("\n", logo, "\nRunning on http://127.0.0.1:8080/v1/weather")
	log.Fatal(http.ListenAndServe(":8080", nil))
}

// retrieveData get two channels from PrimaryProvider and FailoverProvider
// then wait for data from them, before return the data it also sets it into the cache
//
// if valid primary data come back first, return it instantly
// otherwise if valid failover data come back first, keep waiting for the primary data
// if primary data is valid then return it, otherwise return the failover data
func (ws *WeatherServer) retrieveData(city string) (wd WeatherData, err error) {
	primaryChan := ws.PrimaryProvider.GetResChannel(city+"_primary", ws.Pubsub)
	failoverChan := ws.FailoverProvider.GetResChannel(city+"_failover", ws.Pubsub)

	select {
	case itf := <-primaryChan:
		log.Println("primary provider data come back first")
		if wd, err = assertWeatherData(itf); err == nil {
			log.Println("use primary provider data")
		} else {
			otherItf := <-failoverChan
			if wd, err = assertWeatherData(otherItf); err == nil {
				log.Println("use failover provider data")
			}
		}
	case itf := <-failoverChan:
		log.Println("failover provider data come back first")
		otherItf := <-primaryChan
		if wd, err = assertWeatherData(otherItf); err == nil {
			log.Println("use primary provider data")
		} else if wd, err = assertWeatherData(itf); err == nil {
			log.Println("use failover provider data")
		}
	}

	ws.Pubsub.Unsub(primaryChan, city+"_primary")
	ws.Pubsub.Unsub(failoverChan, city+"_failover")

	if wd != (WeatherData{}) {
		log.Println("set data on cache", wd)
		ws.Cache.Set(city+"_fresh", wd, cache.DefaultExpiration)
		ws.Cache.Set(city+"_stale", wd, cache.NoExpiration)
		return
	}
	return
}

// Serve TBD
func (ws *WeatherServer) Serve() {
	ws.runAPIServer()
}

// GetWeather provides weather data in json
// GetWeather try get the data from the following order:
// 1. Hot cache (not older than 3 seconds)
// 2. Primary weather provider
// 3. Failover weather provider
// 4. Cold cache
func (ws *WeatherServer) GetWeather(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	city := "sydney"
	wd := WeatherData{}
	var err error
	if weatherDataItf, found := ws.Cache.Get(city + "_fresh"); found {
		log.Println("getting data from fresh cache")
		wd, _ := weatherDataItf.(WeatherData)
		json.NewEncoder(w).Encode(wd)
		return
	} else if wd, err = ws.retrieveData(city); err == nil {
		json.NewEncoder(w).Encode(wd)
		return
	} else if weatherDataItf, found := ws.Cache.Get(city + "_stale"); found {
		log.Println("getting data from stale cache")
		wd, _ := weatherDataItf.(WeatherData)
		json.NewEncoder(w).Encode(wd)
		return
	}
	log.Println("No data found from cache or provider.", err)
	w.Write([]byte(`{"status": "failure"}`))
}
