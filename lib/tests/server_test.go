package lib

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	lib "weather-app/lib"
	mock_lib "weather-app/lib/mocks"

	"github.com/cskr/pubsub"
	"github.com/golang/mock/gomock"
	"github.com/google/go-cmp/cmp"
	cache "github.com/patrickmn/go-cache"
)

func TestBurstRequests(t *testing.T) {
	dialerReturn := `{"location":{"name":"Sydney","region":"New South Wales","country":"Australia","lat":-33.88,"lon":151.22,"tz_id":"Australia/Sydney","localtime_epoch":1563623881,"localtime":"2019-07-20 21:58"},"current":{"last_updated_epoch":1563622812,"last_updated":"2019-07-20 21:40","temp_c":15.0,"temp_f":59.0,"is_day":0,"condition":{"text":"Clear","icon":"//cdn.apixu.com/weather/64x64/night/113.png","code":1000},"wind_mph":6.9,"wind_kph":11.2,"wind_degree":320,"wind_dir":"NW","pressure_mb":1019.0,"pressure_in":30.6,"precip_mm":0.0,"precip_in":0.0,"humidity":51,"cloud":0,"feelslike_c":14.5,"feelslike_f":58.0,"vis_km":10.0,"vis_miles":6.0,"uv":0.0,"gust_mph":24.8,"gust_kph":40.0}}`
	dialer2Return := `{"coord":{"lon":151.22,"lat":-33.85},"weather":[{"id":800,"main":"Clear","description":"clear sky","icon":"01n"}],"base":"stations","main":{"temp":283.59,"pressure":1019,"humidity":58,"temp_min":278.15,"temp_max":288.15},"visibility":10000,"wind":{"speed":2.6,"deg":310},"clouds":{"all":0},"dt":1563624606,"sys":{"type":1,"id":9600,"message":0.0104,"country":"AU","sunrise":1563569750,"sunset":1563606398},"timezone":36000,"id":2147714,"name":"Sydney","cod":200}`
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	dialer := mock_lib.NewMockDailer(ctrl)
	dialer.EXPECT().Get(gomock.Any()).DoAndReturn(func(url string) ([]byte, error) {
		time.Sleep(40 * time.Millisecond)
		return []byte(dialerReturn), nil
	})

	dialer2 := mock_lib.NewMockDailer(ctrl)
	dialer2.EXPECT().Get(gomock.Any()).DoAndReturn(func(url string) ([]byte, error) {
		time.Sleep(30 * time.Millisecond)
		return []byte(dialer2Return), nil
	})

	primaryProvider, failoverProvider := lib.NewProviders(dialer, dialer2)
	s := &lib.WeatherServer{
		PrimaryProvider:  primaryProvider,
		FailoverProvider: failoverProvider,
		Cache:            cache.New(3*time.Second, time.Minute),
		Pubsub:           pubsub.New(1),
	}
	req := httptest.NewRequest(http.MethodGet, "localhost:8080/v1/weather", nil)
	rec := httptest.NewRecorder()

	s.GetWeather(rec, req)
	for i := 0; i < 5000; i++ {
		go func() {
			req := httptest.NewRequest(http.MethodGet, "localhost:8080/v1/weather", nil)
			rec := httptest.NewRecorder()
			s.GetWeather(rec, req)
		}()
	}
	resp := rec.Result()
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		t.Errorf("expected status code 200; got %v", resp.StatusCode)
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Errorf("failed reading response body; got %v", resp)
	}
	msg := make(map[string]float64)
	err = json.Unmarshal(data, &msg)
	if err != nil {
		t.Errorf("failed in unmarshaling response data; got %v", data)
	}

	expectMsg := map[string]float64{
		"temprature_degrees": 15,
		"wind_speed":         11.2,
	}

	if !cmp.Equal(msg, expectMsg) {
		t.Errorf("Response data fail to match, got: %v, want: %v.", msg, expectMsg)
	}
}

func TestPrimaryProviderFailure(t *testing.T) {
	dialer2Return := `{"coord":{"lon":151.22,"lat":-33.85},"weather":[{"id":800,"main":"Clear","description":"clear sky","icon":"01n"}],"base":"stations","main":{"temp":283.59,"pressure":1019,"humidity":58,"temp_min":278.15,"temp_max":288.15},"visibility":10000,"wind":{"speed":2.6,"deg":310},"clouds":{"all":0},"dt":1563624606,"sys":{"type":1,"id":9600,"message":0.0104,"country":"AU","sunrise":1563569750,"sunset":1563606398},"timezone":36000,"id":2147714,"name":"Sydney","cod":200}`
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	dialer := mock_lib.NewMockDailer(ctrl)
	dialer.EXPECT().Get(gomock.Any()).Return([]byte{}, errors.New("Timeout"))

	dialer2 := mock_lib.NewMockDailer(ctrl)
	dialer2.EXPECT().Get(gomock.Any()).DoAndReturn(func(url string) ([]byte, error) {
		time.Sleep(30 * time.Millisecond)
		return []byte(dialer2Return), nil
	})

	primaryProvider, failoverProvider := lib.NewProviders(dialer, dialer2)
	s := &lib.WeatherServer{
		PrimaryProvider:  primaryProvider,
		FailoverProvider: failoverProvider,
		Cache:            cache.New(3*time.Second, time.Minute),
		Pubsub:           pubsub.New(1),
	}
	req := httptest.NewRequest(http.MethodGet, "localhost:8080/v1/weather", nil)
	rec := httptest.NewRecorder()

	s.GetWeather(rec, req)
	resp := rec.Result()
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		t.Errorf("expected status code 200; got %v", resp.StatusCode)
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Errorf("failed reading response body; got %v", resp)
	}
	msg := make(map[string]float64)
	err = json.Unmarshal(data, &msg)
	if err != nil {
		t.Errorf("failed in unmarshaling response data; got %v", data)
	}

	expectMsg := map[string]float64{
		"temprature_degrees": 283.59,
		"wind_speed":         2.6,
	}

	if !cmp.Equal(msg, expectMsg) {
		t.Errorf("Response data fail to match, got: %v, want: %v.", msg, expectMsg)
	}
}

func TestHitFreshCache(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	dialer := mock_lib.NewMockDailer(ctrl)
	cacher := mock_lib.NewMockCacher(ctrl)
	cacher.EXPECT().Get(gomock.Any()).Return(lib.WeatherData{
		WindSpeed: 3,
		Temp:      3,
	}, true)

	primaryProvider, failoverProvider := lib.NewProviders(dialer, dialer)
	s := &lib.WeatherServer{
		PrimaryProvider:  primaryProvider,
		FailoverProvider: failoverProvider,
		Cache:            cacher,
		Pubsub:           pubsub.New(1),
	}
	req := httptest.NewRequest(http.MethodGet, "localhost:8080/v1/weather", nil)
	rec := httptest.NewRecorder()

	s.GetWeather(rec, req)
	resp := rec.Result()
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		t.Errorf("expected status code 200; got %v", resp.StatusCode)
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Errorf("failed reading response body; got %v", resp)
	}
	msg := make(map[string]float64)
	err = json.Unmarshal(data, &msg)
	if err != nil {
		t.Errorf("failed in unmarshaling response data; got %v", data)
	}

	expectMsg := map[string]float64{
		"temprature_degrees": 3.0,
		"wind_speed":         3.0,
	}

	if !cmp.Equal(msg, expectMsg) {
		t.Errorf("Response data fail to match, got: %v, want: %v.", msg, expectMsg)
	}
}

func TestHitStaleCache(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	dialer := mock_lib.NewMockDailer(ctrl)
	dialer.EXPECT().Get(gomock.Any()).Return([]byte{}, errors.New("Timeout")).Times(2)

	cacher := mock_lib.NewMockCacher(ctrl)
	cacher.EXPECT().Get(gomock.Any()).Return(lib.WeatherData{}, false)
	cacher.EXPECT().Get(gomock.Any()).Return(lib.WeatherData{
		WindSpeed: 3,
		Temp:      3,
	}, true)

	primaryProvider, failoverProvider := lib.NewProviders(dialer, dialer)
	s := &lib.WeatherServer{
		PrimaryProvider:  primaryProvider,
		FailoverProvider: failoverProvider,
		Cache:            cacher,
		Pubsub:           pubsub.New(1),
	}
	req := httptest.NewRequest(http.MethodGet, "localhost:8080/v1/weather", nil)
	rec := httptest.NewRecorder()

	s.GetWeather(rec, req)
	resp := rec.Result()
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		t.Errorf("expected status code 200; got %v", resp.StatusCode)
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Errorf("failed reading response body; got %v", resp)
	}
	msg := make(map[string]float64)
	err = json.Unmarshal(data, &msg)
	if err != nil {
		t.Errorf("failed in unmarshaling response data; got %v", data)
	}

	expectMsg := map[string]float64{
		"temprature_degrees": 3.0,
		"wind_speed":         3.0,
	}

	if !cmp.Equal(msg, expectMsg) {
		t.Errorf("Response data fail to match, got: %v, want: %v.", msg, expectMsg)
	}
}

func TestMissCacheAndProviderFailure(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	dialer := mock_lib.NewMockDailer(ctrl)
	dialer.EXPECT().Get(gomock.Any()).Return([]byte{}, errors.New("Timeout")).Times(2)

	cacher := mock_lib.NewMockCacher(ctrl)
	cacher.EXPECT().Get(gomock.Any()).Return(lib.WeatherData{}, false).Times(2)

	primaryProvider, failoverProvider := lib.NewProviders(dialer, dialer)
	s := &lib.WeatherServer{
		PrimaryProvider:  primaryProvider,
		FailoverProvider: failoverProvider,
		Cache:            cacher,
		Pubsub:           pubsub.New(1),
	}
	req := httptest.NewRequest(http.MethodGet, "localhost:8080/v1/weather", nil)
	rec := httptest.NewRecorder()

	s.GetWeather(rec, req)
	resp := rec.Result()
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		t.Errorf("expected status code 200; got %v", resp.StatusCode)
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Errorf("failed reading response body; got %v", resp)
	}
	msg := make(map[string]string)
	err = json.Unmarshal(data, &msg)
	if err != nil {
		t.Errorf("failed in unmarshaling response data; got %v", data)
	}

	expectMsg := map[string]string{
		"status": "failure",
	}

	if !cmp.Equal(msg, expectMsg) {
		t.Errorf("Response data fail to match, got: %v, want: %v.", msg, expectMsg)
	}
}
