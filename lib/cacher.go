package lib

import "time"

// Cacher TBD
type Cacher interface {
	Set(k string, x interface{}, d time.Duration)
	Get(k string) (interface{}, bool)
}