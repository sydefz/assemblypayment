package lib

import "errors"

// WeatherData TBD
type WeatherData struct {
	WindSpeed float64 `json:"wind_speed"`
	Temp      float64 `json:"temprature_degrees"`
	// Primary   bool    `json:"primary"`
	Primary bool `json:"-"`
}

func assertWeatherData(itf interface{}) (WeatherData, error) {
	if err, ok := itf.(error); ok {
		return WeatherData{}, err
	} else if wd, ok := itf.(WeatherData); ok {
		return wd, nil
	}
	return WeatherData{}, errors.New("unknown format received from channel")
}
