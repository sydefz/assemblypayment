package lib

import (
	"errors"
	"time"

	"github.com/imroc/req"
	"github.com/tidwall/gjson"
)

// Dailer TBD
type Dailer interface {
	Get(url string) ([]byte, error)
}

// HTTPDailer TBD
type HTTPDailer struct {
	timeout int64 // time out in x Millisecond
}

// Get TBD
func (hd *HTTPDailer) Get(url string) (res []byte, err error) {
	req.SetTimeout(time.Duration(hd.timeout) * time.Millisecond)
	if response, e := req.Get(url); e != nil {
		err = e
	} else if res, err = response.ToBytes(); e != nil {
		err = e
	} else if !gjson.ValidBytes(res) {
		err = errors.New("invalid json")
	}
	return
}
