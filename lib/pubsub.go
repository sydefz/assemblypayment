package lib

// PubSuber TBD
type PubSuber interface {
	Pub(msg interface{}, topics ...string)
	SubOnce(topics ...string) chan interface{}
	Unsub(ch chan interface{}, topics ...string)
}
