module weather-app

require (
	github.com/cornelk/hashmap v1.0.0
	github.com/cskr/pubsub v1.0.2
	github.com/golang/mock v1.3.1
	github.com/google/go-cmp v0.3.0
	github.com/gorilla/mux v1.7.3
	github.com/imroc/req v0.2.1
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/tidwall/gjson v1.1.3
	github.com/tidwall/match v0.0.0-20171002075945-1731857f09b1 // indirect
)
