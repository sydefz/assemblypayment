## Overview
This is code test for assembly payment, the program provides a http server with endpoint to fetch weather information for a given city. This service source its information from the either of the below providers:
API XU
Open Weather Map

## Assumptions
* The service will cache weather result for up to 3 seconds
* The service will return stale weather data if both providers went down
* The service use API XU provider and failover to Open Weather Map provider, when primary provider went down it needs to response quickly from failover provider
* The service needs to be able to handle large volume of concurrent requests

## Packages Used
* github.com/cornelk/hashmap is a lock free thread safe Map implementation, I used this to store pending requests on WeatherProvider so we don't make concurrent duplicate requests to our weather provider
* github.com/cskr/pubsub is a multi topic pub-sub tool, I used this to communicate between http handler goroutines and weather provider goroutine.
* github.com/imroc/req is a http request client library, using it to simplify making request to weather providers.
* github.com/patrickmn/go-cache is a in-memory key value store, using it to cache weather data.
* github.com/tidwall/gjson is a json tool, using it extract tempature and wind speed data from json weather provider json response.

## Prerequisites
* Go version 1.12+ OR docker

## Running
* at project root, do `go run main.go` to run weather app server
* if docker is installed, project root do `docker build -t weather-app . && docker run -p 8080:8080 weather-app` to run server in docker container

## Testing
* do `go test ./...` to run the unit tests
* do `curl -s http://127.0.0.1:8080/v1/weather?[1-5]` to send 5 requests in sequence
* do `curl -s http://127.0.0.1:8080/v1/weather & curl -s http://127.0.0.1:8080/v1/weather & curl -s http://127.0.0.1:8080/v1/weather & wait` to send 3 requests concurrently

## TODO
* Load api key from config files
* Replace in-memory cache with Memcache/Redis
* Replace hard code "Sydney" with query
* Instead of using pubsub model, we can have a MutexGroup on server to avoid making concurrent duplicate requests to our weather provider (github.com/GitbookIO/syncgroup)
* JSON logging to a log file
* Adding stats via prometheus
* Better error handling