FROM golang:1.12
WORKDIR /usr/local/weather-app
COPY . .
RUN go build
EXPOSE 8080
ENTRYPOINT ["./weather-app"]
