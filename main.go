package main

import (
	"weather-app/lib"
)

func main() {
	lib.NewWeatherServer().Serve()
}
